import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DogsService {

  currentBreed : any;

  constructor(private httpClient : HttpClient) { }

  public getJSON(url): Observable<any> {
    return this.httpClient.get(url);
  }
}