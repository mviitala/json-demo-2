import { Component} from '@angular/core';
import { DogsService } from '../dogs.service';

@Component({
  selector: 'app-image',
  templateUrl: './image.page.html',
  styleUrls: ['./image.page.scss'],
})
export class ImagePage {

  private URL : string;
  imageURL : any;
  sub: any;
  
  constructor(private dogsService : DogsService) {

    this.URL = "https://dog.ceo/api/breed/hound/" + dogsService.currentBreed + "/images/random";
    
    this.dogsService.getJSON(this.URL).subscribe(data => {
      
      console.log(data);
      this.imageURL = data.message;
     });
  }

}
