import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { DogsService } from '../dogs.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  items: any;
  private URL: string = 'https://dog.ceo/api/breed/hound/list';

  constructor(private dogsService: DogsService, private router: Router) {

    this.dogsService.getJSON(this.URL).subscribe(data => {

      console.log(data);
      this.items = data;
    });
  }

  openImage(item) {

    this.dogsService.currentBreed = item;
    this.router.navigate(["/image"]);

  }

}
